//
//  TermiantionListener.m
//  Copymesh
//
//  Created by Sumesh on 2013-01-01.
//  Copyright (c) 2013 Sumesh. All rights reserved.
//
#import "TerminationListener.h"

@implementation TermiantionListener
-(id) initWithExecutablePath:(const char*)execPath parentProcessId:(pid_t)ppid {
    self = [super init];
    NSLog(@"init");
    if (self) {
        _executablePath = execPath;
        _parentProcessId = ppid;
        
        NSTimer *timeout = [NSTimer scheduledTimerWithTimeInterval:30 target:self selector:@selector(timoutOccurred:) userInfo:nil repeats:NO];
        
        [[NSRunLoop currentRunLoop] addTimer:timeout forMode:NSRunLoopCommonModes];
        
        //This adds i/p src req by run loop
        [[[NSWorkspace sharedWorkspace] notificationCenter] addObserver:self selector:@selector(applicationDidTerminate:) name:NSWorkspaceDidTerminateApplicationNotification object:nil];
        NSLog(@"getppid() = %d", getppid());
        if (getppid() == 1) {
            //ppid is launchd (1) -> parent terminated already
            [self relaunch];
        }
    }
    return self;
}

-(void) timoutOccurred:(id)sender {
    NSAlert* timeoutAlert = [[NSAlert alloc] init];
    [timeoutAlert setMessageText:@"Copymesh could not restart!"];
    [timeoutAlert setInformativeText:@"Please manually start Copymesh.  It'd be fantastic if you could email copymesh@gmail.com the steps to reproduce this issue."];
    [timeoutAlert runModal];
    exit(0);
    
}

-(void)applicationDidTerminate:(NSNotification*)notification {
    NSLog(@"application did terminate");
    if (_parentProcessId == [[[notification userInfo] valueForKey:@"NSApplicationProcessIdentifier"] intValue]) {
        //parent just terminated
        [self relaunch];
    }
}

-(void)relaunch {
    NSLog(@"relaunching");
    [[NSWorkspace sharedWorkspace] launchApplication:[NSString stringWithUTF8String:_executablePath]];
    NSLog(@"relaunched %s",_executablePath);
    exit(0);
}

@end
