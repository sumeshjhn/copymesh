//
//  TermiantionListener.h
//  Copymesh
//
//  Created by Sumesh on 2013-01-01.
//  Copyright (c) 2013 Sumesh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>

@interface TermiantionListener : NSObject {
    const char *_executablePath;
    pid_t _parentProcessId;
}

-(id) initWithExecutablePath:(const char*)execPath parentProcessId:(pid_t)ppid;
@end
