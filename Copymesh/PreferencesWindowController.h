//
//  PreferencesWindowController.h
//  Copymesh
//
//  Created by Sumesh on 2012-12-22.
//  Copyright (c) 2012 Sumesh. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@protocol PreferencesCopymeshSettingsDelegate <NSObject>
-(void)maxNumberOfItemsDecreasedFrom:(int)oldMax To:(int)newMax;
-(void)clearPersistedHistoryHistory;
@end

@interface PreferencesWindowController : NSWindowController

@property (nonatomic, weak) id<PreferencesCopymeshSettingsDelegate> copymeshSettingsDelegate;

#pragma mark - overriden methods
-(id)init;
-(void)showWindow:(id)sender;
@end
