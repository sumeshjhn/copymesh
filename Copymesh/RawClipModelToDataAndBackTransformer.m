//
//  RawClipModelToDataAndBackTransformer.m
//  Copymesh
//
//  Created by Sumesh on 2012-12-10.
//  Copyright (c) 2012 Sumesh. All rights reserved.
//

#import "RawClipModelToDataAndBackTransformer.h"

@implementation RawClipModelToDataAndBackTransformer

+(BOOL)allowsReverseTransformation {
    return YES;
}

+(Class)transformedValueClass {
    return [NSData class];
}

//types the app uses --> NSData
-(id)transformedValue:(id)value {
//    NSData* data = nil;    
//    if ([value isKindOfClass:[NSImage class]]) {
//        NSBitmapImageRep* bitmapRep = [[value representations] objectAtIndex:0];
//        data = [bitmapRep representationUsingType:bitmapRep properties:nil];
//    } else if ([value isKindOfClass:[NSAttributedString class]]) {
//        data = [NSKeyedArchiver archivedDataWithRootObject:value];
//    }
//    return data;
    return [NSKeyedArchiver archivedDataWithRootObject:value];
}

//NSData --> types the app uses
-(id)reverseTransformedValue:(id)value {
    return [NSKeyedUnarchiver unarchiveObjectWithData:value];
}
@end
