//
//  HistoryViewController.h
//  Copymesh
//
//  Created by Sumesh on 2012-12-10.
//  Copyright (c) 2012 Sumesh. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class RawClipModel;

@interface HistoryViewController : NSViewController <NSTableViewDelegate, NSTableViewDataSource>

@property (nonatomic, strong) NSMutableArray* historyArray;
@property (nonatomic, weak) IBOutlet NSTableView *tableView;

- (id)initWithNibName:(NSString *)nibNameOrNil andHistoryArray:(NSMutableArray*)historyArray bundle:(NSBundle *)nibBundleOrNil;
-(void) addItemToHistory:(id)clipRaw;
-(void) deleteObjectsFromStartIndex:(NSUInteger)startIndex toEndIndex:(NSUInteger)endIndex;
-(void) clearHistory;

@property (nonatomic, strong) NSManagedObjectContext* managedObjectContext;

@end
