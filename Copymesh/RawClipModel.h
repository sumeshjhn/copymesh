//
//  RawClipModel.h
//  Copymesh
//
//  Created by Sumesh on 2012-12-16.
//  Copyright (c) 2012 Sumesh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#define RAW_CLIP_MODEL @"RawClipModel"
#define SORT_KEY @"timestamp"
#define RAW_CLIP_KEY @"raw"

@interface RawClipModel : NSManagedObject

@property (nonatomic, retain) id raw;
@property (nonatomic, retain) NSDate * timestamp;

@end
