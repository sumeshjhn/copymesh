//
//  RawClipModel.m
//  Copymesh
//
//  Created by Sumesh on 2012-12-16.
//  Copyright (c) 2012 Sumesh. All rights reserved.
//

#import "RawClipModel.h"


@implementation RawClipModel

@dynamic raw;
@dynamic timestamp;

-(void)awakeFromInsert {
    [super awakeFromInsert];
    self.timestamp = [NSDate date];
}

-(NSString *)description {
    return ([self.raw isKindOfClass:[NSAttributedString class]])?[((NSAttributedString*)self.raw) string]:[NSString stringWithFormat:@"%@",[self.raw class]];
}
@end
