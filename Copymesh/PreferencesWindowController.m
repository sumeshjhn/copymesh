//
//  PreferencesWindowController.m
//  Copymesh
//
//  Created by Sumesh on 2012-12-22.
//  Copyright (c) 2012 Sumesh. All rights reserved.
//

#import "PreferencesWindowController.h"

#import "CopymeshPreferencesViewController.h"

#import "MASShortcutView+UserDefaults.h"
#import "StandardUserDefaultKeys.h"

enum ToolbarItemTags {
    kCopymeshSettings = 0,
    kGeneral = 1,
    kFeedbackAndSupport = 2
    };

@interface PreferencesWindowController () {
    CopymeshPreferencesViewController *_copymeshPreferencesViewController;
}
@end

@implementation PreferencesWindowController

-(id)init {
    self = [super initWithWindowNibName:@"PreferencesWindowController"];
    if (self){
        _copymeshPreferencesViewController = [[CopymeshPreferencesViewController alloc] initWithNibName:@"CopymeshPreferencesViewController" bundle:nil];
    }
    return self;
}

- (void)windowDidLoad
{
    [super windowDidLoad];    
    [self.window setTitle:@"Copymesh"];    
    [self.window setCanHide:NO];
    
    NSButton* closeBtn = [self.window standardWindowButton:NSWindowCloseButton] ;
    
    [closeBtn setTarget:self];
    [closeBtn setAction:@selector(closePreferencesWindow:)];

    [self.window.contentView addSubview:_copymeshPreferencesViewController.view];
}

#pragma mark - public class methods
-(void)showWindow:(id)sender {
    NSMutableDictionary* persistedSettings = [[[NSUserDefaults standardUserDefaults] dictionaryForKey:NSUSERDEFAULTS_DICTIONARY_KEY] mutableCopy];
    
    if ([CopymeshPreferencesViewController getCopymeshFromLoginItemsList] != NULL) {
        [persistedSettings setObject:[NSNumber numberWithInteger:NSOnState] forKey:START_ON_LOGIN_KEY];
    } else {
        [persistedSettings setObject:[NSNumber numberWithInteger:NSOffState] forKey:START_ON_LOGIN_KEY];
    }
    [[NSUserDefaults standardUserDefaults] setObject:persistedSettings forKey:NSUSERDEFAULTS_DICTIONARY_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [_copymeshPreferencesViewController initPreferencesUI];
    [super showWindow:sender];
}

#pragma mark - private class methods
-(void) closePreferencesWindow:(id)sender {

    
    if ([_copymeshPreferencesViewController havePreferencesChanged]){
        NSAlert *alert = [[NSAlert alloc] init];
        
        [alert addButtonWithTitle:@"Save"];
        [alert addButtonWithTitle:@"Discard"];
        [alert addButtonWithTitle:@"Cancel"];
        [alert setMessageText:@"Hold it!"];
        [alert setInformativeText:@"Changes were made.  Should they be discarded?"];
        [alert setAlertStyle:NSWarningAlertStyle];
        
        [alert beginSheetModalForWindow:self.window modalDelegate:self didEndSelector:@selector(alertDidEnd:returnCode:contextInfo:) contextInfo:nil];
    } else {
        [self close];
    }
}

#pragma mark - NSAlertView delegate methods
- (void)alertDidEnd:(NSAlert *)alert returnCode:(NSInteger)returnCode contextInfo:(void *)contextInfo {
    switch (returnCode) {
        case NSAlertFirstButtonReturn:
        {
            NSDictionary* persistedSettings = [[NSUserDefaults standardUserDefaults] dictionaryForKey:NSUSERDEFAULTS_DICTIONARY_KEY];
            int newMax = [[_copymeshPreferencesViewController.settingsUIDictionary objectForKey:MAX_NUMBER_ITEMS_KEY] intValue];
            int oldMax = [[persistedSettings objectForKey:MAX_NUMBER_ITEMS_KEY] intValue];
            
            if (newMax < oldMax) {
                [self.copymeshSettingsDelegate maxNumberOfItemsDecreasedFrom:oldMax To:newMax];
            }
            
            if ([[_copymeshPreferencesViewController.settingsUIDictionary objectForKey:SAVE_HISTORY_KEY] integerValue] == NSOffState) {
                [self.copymeshSettingsDelegate clearPersistedHistoryHistory];
            }
            
            if ([[_copymeshPreferencesViewController.settingsUIDictionary objectForKey:HIDE_DOCK_ICON_KEY] integerValue] != [[persistedSettings objectForKey:HIDE_DOCK_ICON_KEY] integerValue]){
                [_copymeshPreferencesViewController savePreferencesChanges];
                [self close];
                
                NSString *argv1 = [[NSBundle mainBundle] bundlePath];
                NSString *argv2 = [[NSNumber numberWithInt:[[NSProcessInfo processInfo] processIdentifier]] stringValue];
                [NSTask launchedTaskWithLaunchPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"relCopymesh"] arguments:@[argv1, argv2]];
                [NSApp terminate:nil];
            }
            
            [_copymeshPreferencesViewController savePreferencesChanges];
            [self close];
            break;
        }
        case NSAlertSecondButtonReturn:
        {
            [_copymeshPreferencesViewController discardPreferencesChanges];
            [self close];
            break;
        }
        case NSAlertThirdButtonReturn:
        {
            //do nothing
            break;
        }
        default:
            break;
    }
}

#pragma mark - IB Methods
- (IBAction)toolbarItemClicked:(id)sender {
    NSString *windowTitle = [sender label];
    NSViewController *windowView = nil;
    switch ([sender tag]) {
        case kCopymeshSettings:
        {
            windowView = _copymeshPreferencesViewController;
            break;
        }
        case kFeedbackAndSupport:
        {
            windowTitle = @"Feedback And Support";
            break;
        }
        default:
        {
            windowTitle = [sender label];
            break;
        }
    }
    
    [self.window.contentView addSubview:windowView.view];
//    [self.window setTitle:windowTitle];
}

@end
