//
//  AppDelegate.m
//  Copymesh
//
//  Created by Sumesh on 2012-12-10.
//  Copyright (c) 2012 Sumesh. All rights reserved.
//

#import "AppDelegate.h"

#import "ClipUtils.h"
#import "StandardUserDefaultKeys.h"

#import "HistoryViewController.h"
#import "RawClipModel.h"

#import "MASShortcut+UserDefaults.h"

#define DEFAULT_HIDE_SHOW_COPYMESH_KEYCODE 5

typedef enum {
    kShowCopymesh = 0,
    kRemoveAllItems = 1,
    kQuit = 2,
    kPreferences = 3,
    kAbout = 4
} StatusBarMenuItems;

@interface AppDelegate () {
    NSUInteger _previousChangeCount;
    NSStatusItem * _statusItem;
    PreferencesWindowController *_preferencesWindowController;
//    NSMutableArray* _hvcArray;
}
@end

@implementation AppDelegate

@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize managedObjectContext = _managedObjectContext;

//https://developer.apple.com/library/mac/#documentation/Cocoa/Conceptual/CoreData/Articles/cdFetching.html#//apple_ref/doc/uid/TP40002484-SW1

-(void)applicationWillFinishLaunching:(NSNotification *)notification {
    _previousChangeCount = 0;
    _statusItem = nil;

//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:NSUSERDEFAULTS_DICTIONARY_KEY];
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:SHOW_HIDE_SHORTCUT_KEY];
    
    NSDictionary* persistedSettings = [[NSUserDefaults standardUserDefaults] dictionaryForKey:NSUSERDEFAULTS_DICTIONARY_KEY];
    if (!persistedSettings) {
        NSMutableDictionary* defaultSettings = [NSMutableDictionary new];
        
        [defaultSettings setObject:[NSNumber numberWithInt:10] forKey:MAX_NUMBER_ITEMS_KEY];
        
        [defaultSettings setObject:[NSNumber numberWithInteger:NSOnState] forKey:SAVE_HISTORY_KEY];
        [defaultSettings setObject:[NSNumber numberWithInteger:NSOffState] forKey:START_ON_LOGIN_KEY];
        
        [defaultSettings setObject:[NSNumber numberWithInteger:NSOffState] forKey:HIDE_DOCK_ICON_KEY];
        [[NSApplication sharedApplication] setActivationPolicy:NSApplicationActivationPolicyRegular];
        
        [[NSUserDefaults standardUserDefaults] setObject:defaultSettings forKey:NSUSERDEFAULTS_DICTIONARY_KEY];
    } else {
        
        if ([[persistedSettings objectForKey:HIDE_DOCK_ICON_KEY] integerValue] == NSOffState) {
           [[NSApplication sharedApplication] setActivationPolicy:NSApplicationActivationPolicyRegular];
        }
    }
    
    //If the user has not set the shortcut, the default is Cmd-Alt_G
    if (![[NSUserDefaults standardUserDefaults] objectForKey:SHOW_HIDE_SHORTCUT_KEY]) {
        MASShortcut* defaultShortcut = [MASShortcut shortcutWithKeyCode:DEFAULT_HIDE_SHOW_COPYMESH_KEYCODE modifierFlags:NSCommandKeyMask|NSAlternateKeyMask];
        [[NSUserDefaults standardUserDefaults] setObject:defaultShortcut.data forKey:SHOW_HIDE_SHORTCUT_KEY];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    [self initStatusMenuItems];
    if (!AXAPIEnabled() && !AXIsProcessTrusted()) {
        [self makeProcessTrusted];
    }
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    //try making application trusted once more
    if (!AXAPIEnabled() && !AXIsProcessTrusted()) {
        [self makeProcessTrusted];
    } else {
//        for (RawClipModel* rc in [self fetchPresistedArray]) {
//            NSLog(@"rc raw = %@", ([rc.raw isKindOfClass:[NSAttributedString class]])?[((NSAttributedString*)rc.raw) string]:rc.raw);
//        }
        NSMutableArray* arrayM = [[self fetchPresistedArray] mutableCopy];
        self.hVC = [[HistoryViewController alloc] initWithNibName:@"HistoryViewController" andHistoryArray:arrayM bundle:nil];
        [self.hVC setManagedObjectContext:[self managedObjectContext]];
        [self.window.contentView addSubview:self.hVC.view];
        [self.hVC.tableView reloadData];
        
        NSTimer* pollForClipboardChange = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(monitorChangeCount:) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:pollForClipboardChange forMode:NSRunLoopCommonModes];
        
        [MASShortcut registerGlobalShortcutWithUserDefaultsKey:SHOW_HIDE_SHORTCUT_KEY handler:^{
            if ([[NSApplication sharedApplication] isHidden] || ![[NSApplication sharedApplication] isActive]) {
                [self showCopymesh];
            } else {
                [self hideCopymesh];
            }
        }];
    }
}

- (NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender
{
    // Save changes in the application's managed object context before the application terminates.
    
    if (!_managedObjectContext) {
        return NSTerminateNow;
    }
    
    if (![[self managedObjectContext] commitEditing]) {
        NSLog(@"%@:%@ unable to commit editing to terminate", [self class], NSStringFromSelector(_cmd));
        return NSTerminateCancel;
    }
    
    if (![[self managedObjectContext] hasChanges]) {
        return NSTerminateNow;
    }
    
    NSError *error = nil;
    NSInteger stateOfHistoryCheckbox = [[[[NSUserDefaults standardUserDefaults] dictionaryForKey:NSUSERDEFAULTS_DICTIONARY_KEY] objectForKey:SAVE_HISTORY_KEY] integerValue];
    if (stateOfHistoryCheckbox == NSOnState) {
        if (![[self managedObjectContext] save:&error]) {
            
            // Customize this code block to include application-specific recovery steps.
            BOOL result = [sender presentError:error];
            if (result) {
                return NSTerminateCancel;
            }
            
            NSString *question = NSLocalizedString(@"Could not save changes while quitting. Quit anyway?", @"Quit without saves error question message");
            NSString *info = NSLocalizedString(@"Quitting now will lose any changes you have made since the last successful save", @"Quit without saves error question info");
            NSString *quitButton = NSLocalizedString(@"Quit anyway", @"Quit anyway button title");
            NSString *cancelButton = NSLocalizedString(@"Cancel", @"Cancel button title");
            NSAlert *alert = [[NSAlert alloc] init];
            [alert setMessageText:question];
            [alert setInformativeText:info];
            [alert addButtonWithTitle:quitButton];
            [alert addButtonWithTitle:cancelButton];
            
            NSInteger answer = [alert runModal];
            
            if (answer == NSAlertAlternateReturn) {
                return NSTerminateCancel;
            }
        }
    }
    return NSTerminateNow;
}

#pragma mark - private class methods
// Returns the directory the application uses to store the Core Data store file. This code uses a directory named "com.Sumesh.Copymesh" in the user's Application Support directory.
- (NSURL *)applicationFilesDirectory
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *appSupportURL = [[fileManager URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask] lastObject];
    return [appSupportURL URLByAppendingPathComponent:@"com.Sumesh.Copymesh"];
}

-(void) monitorChangeCount:(id)sender {
    if (_previousChangeCount != [[ClipUtils sharedClipboard] changeCount]) {
        
        [self addToHistoryArray];
        
        _previousChangeCount = [[ClipUtils sharedClipboard] changeCount];
    }
//    [self.hVC.tableView reloadData];
}

-(void) addToHistoryArray {
    id clipDat = [ClipUtils getClipboardDataNatively];
    id mostRecentEntry = ([self.hVC.historyArray count] > 0) ? ((RawClipModel*)[self.hVC.historyArray objectAtIndex:0]).raw : nil;
    
    if (![ClipUtils isFirstClipboardItem:clipDat equalToSecond:mostRecentEntry]) {
                
        [self.hVC addItemToHistory:clipDat];
    }
}

- (NSArray*)fetchPresistedArray {
    //initialize array from previous session
    NSFetchRequest* fetchRequest = [NSFetchRequest new];
    
    NSEntityDescription* entity = [NSEntityDescription entityForName:RAW_CLIP_MODEL inManagedObjectContext:[self managedObjectContext]];
    
    NSSortDescriptor* sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:SORT_KEY ascending:NO];
//    NSSortDescriptor* sortDescriptor = [[NSSortDescriptor alloc] initWithKey:SORT_KEY ascending:NO];
    
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    return [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
}

-(void) hideCopymesh {
    [[NSApplication sharedApplication] hide:nil];
}

-(void) showCopymesh {
    [[NSApplication sharedApplication] activateIgnoringOtherApps: YES];
    [[NSApplication sharedApplication] unhide:nil];
}
#pragma mark - Core Data Methods
// Creates if necessary and returns the managed object model for the application.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel) {
        return _managedObjectModel;
    }
	
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Copymesh" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. (The directory for the store is created, if necessary.)
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator) {
        return _persistentStoreCoordinator;
    }
    
    NSManagedObjectModel *mom = [self managedObjectModel];
    if (!mom) {
        NSLog(@"%@:%@ No model to generate a store from", [self class], NSStringFromSelector(_cmd));
        return nil;
    }
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *applicationFilesDirectory = [self applicationFilesDirectory];
    NSError *error = nil;
    
    NSDictionary *properties = [applicationFilesDirectory resourceValuesForKeys:@[NSURLIsDirectoryKey] error:&error];
    
    if (!properties) {
        BOOL ok = NO;
        if ([error code] == NSFileReadNoSuchFileError) {
            ok = [fileManager createDirectoryAtPath:[applicationFilesDirectory path] withIntermediateDirectories:YES attributes:nil error:&error];
        }
        if (!ok) {
            [[NSApplication sharedApplication] presentError:error];
            return nil;
        }
    } else {
        if (![[properties objectForKey:NSURLIsDirectoryKey] boolValue]) {
            // Customize and localize this error.
            NSString *failureDescription = [NSString stringWithFormat:@"Expected a folder to store application data, found a file (%@).", [applicationFilesDirectory path]];
            
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            [dict setValue:failureDescription forKey:NSLocalizedDescriptionKey];
            error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:101 userInfo:dict];
            
            [[NSApplication sharedApplication] presentError:error];
            return nil;
        }
    }
    
    NSURL *url = [applicationFilesDirectory URLByAppendingPathComponent:@"Copymesh.storedata"];
    NSPersistentStoreCoordinator *coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:mom];
    if (![coordinator addPersistentStoreWithType:NSXMLStoreType configuration:nil URL:url options:nil error:&error]) {
        [[NSApplication sharedApplication] presentError:error];
        return nil;
    }
    _persistentStoreCoordinator = coordinator;
    
    return _persistentStoreCoordinator;
}

// Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) 
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setValue:@"Failed to initialize the store" forKey:NSLocalizedDescriptionKey];
        [dict setValue:@"There was an error building up the data file." forKey:NSLocalizedFailureReasonErrorKey];
        NSError *error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        [[NSApplication sharedApplication] presentError:error];
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];

    return _managedObjectContext;
}

// Returns the NSUndoManager for the application. In this case, the manager returned is that of the managed object context for the application.
- (NSUndoManager *)windowWillReturnUndoManager:(NSWindow *)window
{
    return [[self managedObjectContext] undoManager];
}

// Performs the save action for the application, which is to send the save: message to the application's managed object context. Any encountered errors are presented to the user.
- (IBAction)saveAction:(id)sender
{
    NSError *error = nil;
    
    if (![[self managedObjectContext] commitEditing]) {
        NSLog(@"%@:%@ unable to commit editing before saving", [self class], NSStringFromSelector(_cmd));
    }
    
    if (![[self managedObjectContext] save:&error]) {
        [[NSApplication sharedApplication] presentError:error];
    }
}

#pragma mark - Accessibilty related methods
-(NSString*) getAXErrorEnumString:(AXError)i {
    switch (i) {
        case kAXErrorSuccess:
            return @"Success";
            break;
        case kAXErrorFailure:
            return @"Failure";
        case kAXErrorIllegalArgument:
            return @"Illegal Argument";
            break;
        case kAXErrorInvalidUIElement:
            return @"Invalid UI Element";
            break;
        case kAXErrorInvalidUIElementObserver:
            return @"Invalid UI Element Observer";
            break;
        case kAXErrorCannotComplete:
            return @"Cannot Complete";
            break;
        case kAXErrorAttributeUnsupported:
            return @"ATTRIBUTE Unsupported";
            break;
        case kAXErrorActionUnsupported:
            return @"ACTION Unsupported";
            break;
        case kAXErrorNotificationUnsupported:
            return @"NOTIFICATION Unsupported";
            break;
        case kAXErrorNotImplemented:
            return @"Not Implemented";
            break;
        case kAXErrorNotificationAlreadyRegistered:
            return @"Notification ALREADY Registered";
            break;
        case kAXErrorNotificationNotRegistered:
            return @"Notification NOT Registered";
            break;
        case kAXErrorAPIDisabled:
            return @"API Disabled";
            break;
        case kAXErrorNoValue:
            return @"No Value";
            break;
        case kAXErrorParameterizedAttributeUnsupported:
            return @"Parameterized Attribute Unsupported";
            break;
        case kAXErrorNotEnoughPrecision:
            return @"Not Enough Precision";
            break;
        default:
            return @"Unknown Error";
            break;
    }
}

#pragma mark - Accessibility methods for shortcuts
- (void)makeProcessTrusted;
{
	NSString *makeProcessTrustedAgentPath = [[NSBundle mainBundle] bundlePath];
	
    //we cast NSString to __bridge CFStringRef to not worry about memory management of the core foundation string
    AXError result = AXMakeProcessTrusted((__bridge CFStringRef)(makeProcessTrustedAgentPath));
	[ClipUtils logger:@"result of making %@ trusted = %@",makeProcessTrustedAgentPath,[self getAXErrorEnumString:result]];
    if (result != kAXErrorSuccess)
	{
		[self askUserToEnableAccessForAssistiveDevices];
		return;
	}
	
	//due to a bug with AXMakeProcessTrusted(), we need to be relaunched before we will actually have access to UI Scripting
	[NSApp terminate:nil];
}

- (void)askUserToEnableAccessForAssistiveDevices;
{
	//fallback: ask the user to enable access for assistive devices manually
	NSInteger result = NSRunAlertPanel(@"Enable Access for Assistive Devices." , @"To continue, please enable access for assistive devices in the Universal Access pane in System Preferences. Then, relaunch the application." , @"Open System Preferences", @"Quit", nil);
	
	if(result == NSAlertDefaultReturn)
	{
		[[NSWorkspace sharedWorkspace]openFile:@"/System/Library/PreferencePanes/UniversalAccessPref.prefPane"];
	}
	
	//nothing else we can do right now
	[NSApp terminate:nil];
}

#pragma mark - menu bar methods
-(void) initStatusMenuItems {
    _statusItem = [[NSStatusBar systemStatusBar] statusItemWithLength:NSVariableStatusItemLength];
    [_statusItem setMenu:_statusBarMenu];
    [_statusItem setImage:[NSImage imageNamed:@"statusIcon_UP"]];
    [_statusItem setAlternateImage:[NSImage imageNamed:@"statusIcon_DOWN"]];
    [_statusItem setHighlightMode:YES];
    
    for (NSMenuItem* mI  in [_statusBarMenu itemArray]) {
        if (![mI isSeparatorItem]) {
            //Tags are set in Interface Builder, and the StatusBarMenuItem
            //enum reflects those tags as well
            [mI setAction:@selector(menuItemClicked:)];
        }
    }
}

-(void)menuItemClicked:(id)sender {
    NSMenuItem* nMI = (NSMenuItem*)sender;
    switch (nMI.tag) {
        case kShowCopymesh: {
            [self showCopymesh];
            break;
        }
        case kRemoveAllItems: {
            for (RawClipModel* rcm in self.hVC.historyArray) {
                [self.managedObjectContext deleteObject:rcm];
            }
            [self.hVC clearHistory];
            break;
        }
        case kQuit: {
            [NSApp terminate:nil];
            break;
        }
        case kPreferences: {
            if (!_preferencesWindowController) {
                _preferencesWindowController = [[PreferencesWindowController alloc] init];
                _preferencesWindowController.copymeshSettingsDelegate = self;
            }    
            
            [_preferencesWindowController window];
            [_preferencesWindowController showWindow:nil];
            break;
        }
        case kAbout: {
            NSLog(@"Show about");
            break;
        }
        default:
            break;
    }
}

#pragma mark - PreferencesCopymeshSettingsDelegate delegate methods
-(void)maxNumberOfItemsDecreasedFrom:(int)oldMax To:(int)newMax {
    //delete from persistent storage
    for (int i = newMax; i < [self.hVC.historyArray count]; i++) {
        RawClipModel* rCM = [self.hVC.historyArray objectAtIndex:i];
        [self.managedObjectContext deleteObject:rCM];
    }
    
    //delete from historyviewcontroller
    NSUInteger startIndex = newMax;
    NSUInteger endIndex = [self.hVC.historyArray count];
    
    [self.hVC deleteObjectsFromStartIndex:startIndex toEndIndex:endIndex];
}

-(void)clearPersistedHistoryHistory {
    for (RawClipModel* rcm in self.hVC.historyArray) {
        [self.managedObjectContext deleteObject:rcm];
    }
    
    NSError *error = nil;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"couldn't save after unchecking save history after quitting, error = %@ ", error);
    }
}

@end
