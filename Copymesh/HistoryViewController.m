//
//  HistoryViewController.m
//  Copymesh
//
//  Created by Sumesh on 2012-12-10.
//  Copyright (c) 2012 Sumesh. All rights reserved.
//

#import "HistoryViewController.h"

#import "ClipUtils.h"

#import "RawClipModel.h"
#import "ClipboardModel.h"

#import "StandardUserDefaultKeys.h"

#define TEXT_IMG [NSImage imageNamed:@"textImage"]
#define DATA_IMG [NSImage imageNamed:@"dataImage"]
#define UNKNOWN_IMG [NSImage imageNamed:@"unknownImage"]

#define EMPTY_CLIP @"Empty"

#define MAX_TEXTFIELD_LENGTH 50

@interface HistoryViewController () {
    RawClipModel* _previouslySelectedItem;
}
@end

@implementation HistoryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil andHistoryArray:(NSMutableArray*)historyArray bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
//        for (RawClipModel* rc in historyArray) {
//            NSLog(@"rc raw = %@", ([rc.raw isKindOfClass:[NSAttributedString class]])?[((NSAttr ibutedString*)rc.raw) string]:rc.raw);
//        }
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.historyArray = ([historyArray count] > 0 )?historyArray:[NSMutableArray new];
        _previouslySelectedItem = nil;
    }
    return self;
}

#pragma mark - public class methods
-(void) addItemToHistory:(id)clipRaw {
    
    if (![ClipUtils isFirstClipboardItem:_previouslySelectedItem.raw equalToSecond:clipRaw]) {
        NSUInteger newRowIndex = 0;
        
        NSDictionary* persistedSettings = [[NSUserDefaults standardUserDefaults] dictionaryForKey:NSUSERDEFAULTS_DICTIONARY_KEY];

        RawClipModel* rCM = [NSEntityDescription insertNewObjectForEntityForName:RAW_CLIP_MODEL inManagedObjectContext:[self managedObjectContext]];

        [rCM setValue:clipRaw forKey:RAW_CLIP_KEY];
        
        if ([[persistedSettings objectForKey:SAVE_HISTORY_KEY] integerValue] == NSOnState) {
        
            NSError* error = nil;
            
            if (![[self managedObjectContext] save:&error]) {
                [ClipUtils logger:@"Couldn't persist %@", clipRaw];
            }
        }
        
        if ([self.historyArray count] >= [[persistedSettings objectForKey:MAX_NUMBER_ITEMS_KEY] intValue]) {

            [self.managedObjectContext deleteObject:[self.historyArray lastObject]];
            [self.tableView removeRowsAtIndexes:[NSIndexSet indexSetWithIndex:[self.historyArray count]-1] withAnimation:NSTableViewAnimationEffectFade];
            [self.historyArray removeLastObject];
        }
        
        [self.historyArray insertObject:rCM atIndex:newRowIndex];
        [self.tableView insertRowsAtIndexes:[NSIndexSet indexSetWithIndex:newRowIndex] withAnimation:YES];
    }
}

-(void) deleteObjectsFromStartIndex:(NSUInteger)startIndex toEndIndex:(NSUInteger)endIndex {

    //delete from tableview
    [self.tableView removeRowsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(startIndex, endIndex-startIndex)] withAnimation:NSTableViewAnimationEffectFade];
    
    //delete from array
    [self.historyArray removeObjectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(startIndex, endIndex-startIndex)]];
}

-(void) clearHistory {
    [self.historyArray removeAllObjects];
    [self.tableView reloadData];
    _previouslySelectedItem = nil;
}

#pragma mark - private class methods
-(NSString*) getEmptyTextFromHistoryArrayObject:(id)obj {
    if ([obj isKindOfClass:[NSImage class]]) {
        return @"Image";
    } else if (([obj isKindOfClass:[NSAttributedString class]])) {
        return @"Text";
    } else if ([obj isKindOfClass:[NSData class]]) {
        return @"Data";
    } else {
        return @"Unknown";
    }
}

-(NSImage*) getEmptyImageFromHistoryArrayObject:(id)obj {
    if (([obj isKindOfClass:[NSAttributedString class]] || [obj isKindOfClass:[NSString class]])) {
        return TEXT_IMG;
    } else if ([obj isKindOfClass:[NSData class]]) {
        return DATA_IMG;
    } else {
        return UNKNOWN_IMG;
    }
}

-(NSAttributedString*) getEllipsizedAttributedStringFromAttributedString:(NSAttributedString*)attStr {
    if ([attStr length] > MAX_TEXTFIELD_LENGTH) {
        NSAttributedString* ellipsis = [[NSAttributedString alloc] initWithString:@"..."];
        NSMutableAttributedString* mutAttStr = [[attStr attributedSubstringFromRange:NSRangeFromString([NSString stringWithFormat:@"{0,%i}", MAX_TEXTFIELD_LENGTH])] mutableCopy];
        [mutAttStr insertAttributedString:ellipsis atIndex:[mutAttStr.string length]];
        return mutAttStr;
    }
    return attStr;
}

-(BOOL) indexWithinHistoryArrayBounds:(int)i {
    return (0<=i)&&(i<[self.historyArray count]);
}

#pragma mark - table view delegate methods
-(NSView*)tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
    //get cell view
    NSTableCellView *cellView = [tableView makeViewWithIdentifier:tableColumn.identifier owner:self];
    
    //only modify the history column, which should be the only case regardless
    if ([tableColumn.identifier isEqualToString:@"HistoryColumn"]) {
        id raw = ((RawClipModel*)[self.historyArray objectAtIndex:row]).raw;        
        ClipboardModel* cellModel = [ClipUtils getImageAndTitleFromRaw:raw];
        
        if (!cellModel.thumb && !cellModel.title) {
            cellView.imageView.image = [self getEmptyImageFromHistoryArrayObject:raw];
            cellView.textField.stringValue = [self getEmptyTextFromHistoryArrayObject:raw];
            cellView.textField.textColor = [NSColor grayColor];
        } else {
            if (!cellModel.thumb) {
                cellView.imageView.image = [self getEmptyImageFromHistoryArrayObject:raw];
            } else {
                cellView.imageView.image = cellModel.thumb;
            }
            
            if (!cellModel.title) {
                cellView.textField.stringValue = [self getEmptyTextFromHistoryArrayObject:raw];
                cellView.textField.textColor = [NSColor grayColor];
            } else {
                cellView.textField.attributedStringValue = [self getEllipsizedAttributedStringFromAttributedString:cellModel.title];
            }
        }
    }
    return cellView;
}

-(void) tableViewSelectionDidChange:(NSNotification *)notification {
    int selRow = (int)([self.tableView selectedRow]);
    if ([self indexWithinHistoryArrayBounds:selRow]) {
            _previouslySelectedItem = [self.historyArray objectAtIndex:selRow];        
            [[ClipUtils sharedClipboard] clearContents];
        
            [[ClipUtils sharedClipboard] writeObjects:@[_previouslySelectedItem.raw]];
    }
}

#pragma mark - table view datasource methods
//- (id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
//    //only modify the history column, which should be the only case regardless
//    if ([tableColumn.identifier isEqualToString:@"HistoryColumn"]) {
//        return ([self.historyArray count] > 0) ? [self.historyArray objectAtIndex:row] : nil;
//    }
//    
//    return nil;
//}
- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView {
    return [self.historyArray count];
}

#pragma mark - remove from model and persistant storage
- (IBAction)removeSelectedItem:(id)sender {
    int selRow = (int)([self.tableView selectedRow]);
    if ([self indexWithinHistoryArrayBounds:selRow]) {        
        [ self.managedObjectContext deleteObject:[self.historyArray objectAtIndex:selRow]];
        [self.historyArray removeObjectAtIndex:selRow];
        [self.tableView deselectRow:selRow];
        [self.tableView removeRowsAtIndexes:[NSIndexSet indexSetWithIndex:selRow] withAnimation:NSTableViewAnimationSlideUp];
    }
}

@end