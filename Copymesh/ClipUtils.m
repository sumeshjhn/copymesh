//
//  ClipUtils.m
//  Copymesh
//
//  Created by Sumesh on 2012-11-13.
//  Copyright (c) 2012 Sumesh. All rights reserved.
//

#import "ClipUtils.h"
#import "ClipboardModel.h"

#define LOG_IT 1

static NSPasteboard * pasteboard = nil;
static NSDictionary *defaultAttribuetdStringDIctionary = nil;

@implementation ClipUtils

+(NSPasteboard*)sharedClipboard {
    if (!pasteboard) {
        pasteboard = [NSPasteboard generalPasteboard];
    }
    return pasteboard;
}

+(id) getClipboardDataNatively {

    NSArray *classArray = @[[NSAttributedString class], [NSImage class]];
    NSDictionary *options = [NSDictionary dictionary];
    
    NSArray *objectsToPaste = nil;
    BOOL ok = [[ClipUtils sharedClipboard] canReadObjectForClasses:classArray options:options];
    if (ok) {
        objectsToPaste = [[ClipUtils sharedClipboard] readObjectsForClasses:classArray options:options];
    }
    
    if ([[objectsToPaste objectAtIndex:0] isKindOfClass:[NSAttributedString class]]){
        //if the font attribute doesn't exist, then it clearly does not have any attributes, so give it default attributes
        if ([[[((NSAttributedString*)[objectsToPaste objectAtIndex:0]) fontAttributesInRange:NSMakeRange(0, 1)] allKeys] count] == 0) {
            NSString* string = [[objectsToPaste objectAtIndex:0] string];
            return string;
        }
    }
    return [objectsToPaste objectAtIndex:0];
}

+(ClipboardModel*) getImageAndTitleFromRaw:(id)clipDat {
    if (clipDat) {
        NSAttributedString* title = nil;
        NSImage* thumb = nil;
        if ([clipDat isKindOfClass:[NSImage class]]) {
            title = nil;
            thumb = ((NSImage*)clipDat);
            
            [ClipUtils logger:@"Image = %@", ((NSImage*)clipDat)];
        } else if ([clipDat isKindOfClass:[NSAttributedString class]]) {
            title = ((NSAttributedString*)clipDat);
            thumb = nil;
            
            [ClipUtils logger:@"NSAttibutedString = %@", ((NSAttributedString*)clipDat).string];
        } else if ([clipDat isKindOfClass:[NSString class]]) {
            title = [[NSAttributedString alloc] initWithString:clipDat];
            thumb = nil;
            
            [ClipUtils logger:@"NSString = %@", clipDat];
        } else {
            [ClipUtils logger:@"NSData = %@", ((NSString*)clipDat)];
        }
        return [[ClipboardModel alloc] initWithNSAttributedString:title andThumbnail:thumb];
        
    }
    return nil;
}

+(BOOL) isFirstClipboardItem:(id)first equalToSecond:(id)second {
    BOOL result = NO;
    if ([first isKindOfClass:[NSString class]] && [second isKindOfClass:[NSString class]]) {
        return [first isEqualToString:second];
    } else if ([first isKindOfClass:[NSAttributedString class]] && [second isKindOfClass:[NSAttributedString class]]) {
        result = [first isEqualToAttributedString:second];
        
    } else if (([first isKindOfClass:[NSImage class]] && [second isKindOfClass:[NSImage class]])) {
        
        result = [[((NSImage*)first) TIFFRepresentation] isEqualToData:[((NSImage*)second) TIFFRepresentation]];
        
    } else if ([first isKindOfClass:[NSData class]] && [second isKindOfClass:[NSData class]]) {
        result = [first isEqualToData:second];
        
    }
    return result;
}

+(void) logger:(NSString*)format, ... {
    if (LOG_IT==1) {
        va_list args;
        va_start(args, format);
        NSString *s = [[NSString alloc] initWithFormat:format arguments:args];
        NSLog(@"%@", s);
    }
}

#pragma mark - private class methods
+(NSDictionary*) getDefaultAttributesWithString:(NSString*)string {
    if (defaultAttribuetdStringDIctionary) {
        return defaultAttribuetdStringDIctionary;
    }
    
    defaultAttribuetdStringDIctionary = @{
    NSFontAttributeName : [NSFont systemFontOfSize:12.0f],
    NSParagraphStyleAttributeName : [NSParagraphStyle defaultParagraphStyle]
    };
    return  defaultAttribuetdStringDIctionary;
}
@end
