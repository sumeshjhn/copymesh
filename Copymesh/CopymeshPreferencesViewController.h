//
//  CopymeshPreferencesViewController.h
//  Copymesh
//
//  Created by Sumesh on 2012-12-22.
//  Copyright (c) 2012 Sumesh. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "PreferencesWindowController.h"

@class MASShortcutView, MASShortcut;

@interface CopymeshPreferencesViewController : NSViewController <NSTextFieldDelegate>

@property (weak) IBOutlet NSComboBox *numberOfItemsDropdown;
@property (weak) IBOutlet MASShortcutView *shortcutView;


@property (weak) IBOutlet NSButton *saveHistoryCheckbox;
@property (weak) IBOutlet NSTextField *saveHistoryInfoLabel;
@property (weak) IBOutlet NSButton *startOnLoginCheckbox;
@property (weak) IBOutlet NSButton *hideDockIconCheckbox;
@property (weak) IBOutlet NSTextField *hideDockIconInfoLabel;

@property (nonatomic, strong) MASShortcut *persistedShortcut;
@property (nonatomic, strong) NSMutableDictionary* settingsUIDictionary;

-(void) initPreferencesUI;

-(void) savePreferencesChanges;
-(void) discardPreferencesChanges;
-(BOOL) havePreferencesChanged;
+(LSSharedFileListItemRef) getCopymeshFromLoginItemsList;
@end
