//
//  ClipboardModel.m
//  Copymesh
//
//  Created by Sumesh on 2012-11-13.
//  Copyright (c) 2012 Sumesh. All rights reserved.
//

#import "ClipboardModel.h"

@implementation ClipboardModel
-(id) initWithNSAttributedString:(NSAttributedString*)title andThumbnail:(NSImage*)thumb {
    if (self = [super init]) {
        self.title = title;
        self.thumb = thumb;
    }
    return self;
}
-(NSString *) description {
    return [NSString stringWithFormat:@"\ntitle = %@\nthumb = %@", self.title.string, self.thumb];
}
@end
