//
//  main.m
//  Copymesh
//
//  Created by Sumesh on 2012-12-10.
//  Copyright (c) 2012 Sumesh. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
