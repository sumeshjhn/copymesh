//
//  AppDelegate.h
//  Copymesh
//
//  Created by Sumesh on 2012-12-10.
//  Copyright (c) 2012 Sumesh. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "PreferencesWindowController.h"

@class HistoryViewController;

@interface AppDelegate : NSObject <NSApplicationDelegate, PreferencesCopymeshSettingsDelegate>
@property (assign) IBOutlet NSWindow *window;

@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (nonatomic, strong) HistoryViewController *hVC;
@property (weak) IBOutlet NSMenu *statusBarMenu;

- (IBAction)saveAction:(id)sender;

@end
