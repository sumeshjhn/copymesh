//
//  CopymeshPreferencesViewController.m
//  Copymesh
//
//  Created by Sumesh on 2012-12-22.
//  Copyright (c) 2012 Sumesh. All rights reserved.

#import "CopymeshPreferencesViewController.h"

#import "StandardUserDefaultKeys.h"

#import "MASShortcutView+UserDefaults.h"
#import "MASShortcut+UserDefaults.h"

@interface CopymeshPreferencesViewController (){
    
}
@end

@implementation CopymeshPreferencesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        self.persistedShortcut = [MASShortcut shortcutWithData:[[NSUserDefaults standardUserDefaults] dataForKey:SHOW_HIDE_SHORTCUT_KEY]];
        self.settingsUIDictionary = [[NSMutableDictionary alloc] initWithDictionary:[[NSUserDefaults standardUserDefaults] dictionaryForKey:NSUSERDEFAULTS_DICTIONARY_KEY]];
    }
    
    return self;
}

#pragma mark - public class methods
-(void) initPreferencesUI {
    self.settingsUIDictionary = [[NSMutableDictionary alloc] initWithDictionary:[[NSUserDefaults standardUserDefaults] dictionaryForKey:NSUSERDEFAULTS_DICTIONARY_KEY]];    
    if (self.settingsUIDictionary) {
        [_numberOfItemsDropdown setIntValue:[[self.settingsUIDictionary objectForKey:MAX_NUMBER_ITEMS_KEY] intValue]];
        
        //must be NSOnState or NSOffState
        [_saveHistoryCheckbox setState:[[self.settingsUIDictionary objectForKey:SAVE_HISTORY_KEY] integerValue]];
        [_startOnLoginCheckbox setState:[[self.settingsUIDictionary objectForKey:START_ON_LOGIN_KEY] integerValue]];
        [_hideDockIconCheckbox setState:[[self.settingsUIDictionary objectForKey:HIDE_DOCK_ICON_KEY] integerValue]];
    }
    self.persistedShortcut = [MASShortcut shortcutWithData:[[NSUserDefaults standardUserDefaults] dataForKey:SHOW_HIDE_SHORTCUT_KEY]];
    self.shortcutView.associatedUserDefaultsKey = SHOW_HIDE_SHORTCUT_KEY;
}

-(void) savePreferencesChanges {
    [[NSUserDefaults standardUserDefaults] setObject:self.settingsUIDictionary forKey:NSUSERDEFAULTS_DICTIONARY_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void) discardPreferencesChanges {
    [[NSUserDefaults standardUserDefaults] setObject:self.persistedShortcut.data forKey:SHOW_HIDE_SHORTCUT_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(BOOL) havePreferencesChanged {
    NSDictionary* persistedSettings = [[NSUserDefaults standardUserDefaults] dictionaryForKey:NSUSERDEFAULTS_DICTIONARY_KEY];
return ![self.settingsUIDictionary isEqualToDictionary:persistedSettings] || ![self.persistedShortcut.data isEqualToData:self.shortcutView.shortcutValue.data];
}

+(LSSharedFileListItemRef) getCopymeshFromLoginItemsList {
    NSString* appPath = [[NSBundle mainBundle] bundlePath];
    
    CFURLRef url = (CFURLRef)CFBridgingRetain([NSURL fileURLWithPath:appPath]);
    
    LSSharedFileListRef loginItems = LSSharedFileListCreate(NULL,
                                                            kLSSharedFileListSessionLoginItems,
                                                            NULL);
    if (loginItems) {
        UInt32 seedValue;
        
        NSArray* loginItemsArray = (NSArray*)CFBridgingRelease(LSSharedFileListCopySnapshot(loginItems, &seedValue));
        for (int i = 0; i< [loginItemsArray count]; i++) {
            LSSharedFileListItemRef itemRef = (LSSharedFileListItemRef)CFBridgingRetain([loginItemsArray objectAtIndex:i]);
            if (LSSharedFileListItemResolve(itemRef, 0, (CFURLRef*)&url, NULL) == noErr) {
                NSString* urlPath = [(NSURL*)CFBridgingRelease(url) path];
                if ([urlPath compare:appPath] == NSOrderedSame) {
                    return itemRef;
                }
            }
        }
    }
    
    CFRelease(loginItems);
    return NULL;
}
#pragma mark - private class methods
//consider using this instead
//login items tutorial: http://www.delitestudio.com/2011/10/25/start-dockless-apps-at-login-with-app-sandbox-enabled/
//sandboxing tutoral: https://developer.apple.com/library/mac/#documentation/Security/Conceptual/AppSandboxDesignGuide/AppSandboxQuickStart/AppSandboxQuickStart.html#//apple_ref/doc/uid/TP40011183-CH2-SW3

-(void)addToLoginItems {
	NSString * appPath = [[NSBundle mainBundle] bundlePath];
    
    CFURLRef url = (CFURLRef)CFBridgingRetain([NSURL fileURLWithPath:appPath]);
    
    LSSharedFileListRef loginItems = LSSharedFileListCreate(NULL,
                                                            kLSSharedFileListSessionLoginItems,
                                                            NULL);
    
    if (loginItems) {
        LSSharedFileListItemRef item = LSSharedFileListInsertItemURL(loginItems,
                                                                     kLSSharedFileListItemLast,
                                                                     NULL, NULL, url, NULL, NULL);
        if (item) {
            CFRelease(item);
        }
    }
    CFRelease(loginItems);
}

-(void)removeFromLoginItems {
    LSSharedFileListRef loginItems = LSSharedFileListCreate(NULL,
                                                            kLSSharedFileListSessionLoginItems,
                                                            NULL);
    LSSharedFileListItemRef itemRef = [CopymeshPreferencesViewController getCopymeshFromLoginItemsList];
    if (itemRef != NULL) {
        LSSharedFileListItemRemove(loginItems, itemRef);
        CFRelease(itemRef);
    }
    CFRelease(loginItems);
}

#pragma mark - IB action methods
- (IBAction)hideDockIconChecked:(id)sender {
    NSButton* checkbox = (NSButton*)sender;
    
//    if ([checkbox state] == NSOnState) {
//        [[NSApplication sharedApplication] setActivationPolicy:NSApplicationActivationPolicyProhibited];
//        
//    } else if ([checkbox state] == NSOffState) {
//        [[NSApplication sharedApplication] setActivationPolicy:NSApplicationActivationPolicyRegular];
//    }
    
    NSDictionary* persistedSettings = [[NSUserDefaults standardUserDefaults] dictionaryForKey:NSUSERDEFAULTS_DICTIONARY_KEY];
    
    if ([[persistedSettings objectForKey:HIDE_DOCK_ICON_KEY] integerValue] != [checkbox state]) {
        [self.hideDockIconInfoLabel setHidden:NO];
    } else {
        [self.hideDockIconInfoLabel setHidden:YES];
    }
    
    [self.settingsUIDictionary setObject:[NSNumber numberWithInteger:[checkbox state]] forKey:HIDE_DOCK_ICON_KEY];
}

- (IBAction)startOnLoginChecked:(id)sender {
    NSButton* checkbox = (NSButton*)sender;
    
    if ([checkbox state] == NSOnState) {
        [self addToLoginItems];
    } else if ([checkbox state] == NSOffState) {
        [self removeFromLoginItems];
    }    

    [self.settingsUIDictionary setObject:[NSNumber numberWithInteger:[checkbox state]] forKey:START_ON_LOGIN_KEY];
}

- (IBAction)saveHistoryAfterQuittingChecked:(id)sender {
    NSButton* checkbox = (NSButton*)sender;
    
    if ([checkbox state] == NSOnState) {
        [self.saveHistoryInfoLabel setHidden:YES];
    } else if ([checkbox state] == NSOffState) {
        [self.saveHistoryInfoLabel setHidden:NO];
    }    
    
    [self.settingsUIDictionary setObject:[NSNumber numberWithInteger:[checkbox state]] forKey:SAVE_HISTORY_KEY];
}
- (IBAction)maxItemsChanged:(id)sender {
    [self.settingsUIDictionary setObject:[NSNumber numberWithInt:[(NSComboBox*)sender intValue]]
                      forKey:MAX_NUMBER_ITEMS_KEY];
}

@end
