//
//  ClipboardModel.h
//  Copymesh
//
//  Created by Sumesh on 2012-11-13.
//  Copyright (c) 2012 Sumesh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ClipboardModel : NSObject
@property (nonatomic, strong) NSAttributedString *title;
@property (nonatomic, strong) NSImage* thumb;

-(id) initWithNSAttributedString:(NSAttributedString*)title andThumbnail:(NSImage*)thumb;
@end
