//
//  ClipUtils.h
//  Copymesh
//
//  Created by Sumesh on 2012-11-13.
//  Copyright (c) 2012 Sumesh. All rights reserved.
//

#import <Foundation/Foundation.h>
@class ClipboardModel;

@interface ClipUtils : NSObject
//clipboard related
+(NSPasteboard*)sharedClipboard;
+(id) getClipboardDataNatively;
+(ClipboardModel*) getImageAndTitleFromRaw:(id)clipDat;
+(BOOL) isFirstClipboardItem:(id)first equalToSecond:(id)second;

//Misc
+(void) logger:(NSString*)format, ...;
@end
